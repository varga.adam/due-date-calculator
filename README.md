# DueDateCalculator

The program reads the currently reported problems (bugs) from an issue tracking system and calculates the due date.

## Tests

To run the tests:

- npm install
- npm test

## DueDateCalculator API reference

> constructor(options): DueDateCalculator

- `options: Object`

  - `startTime: string` **(default: 9:00)**

    The begining of the work time. The required format is `hh:mm`

  - `endTime: string` **(default: 17:00)**

    The end of the work time. The required format is `hh:mm`

> The difference of the `startTime` and `endTime` must be 8 hour

```javascript
const dueDateCalculator = new DueDateCalculator({
  startTime: '8:00',
  endTime: '16:00'
});
```

### Returns

- `report(options): function`

  Takes a submit date, turnaround time and turnaround time type as an input and returns the date and time when the issue is to be resolved.

  - `options: Object`

    - `name: string` **(required)**

      The name of the issue

    - `submitDate: Date` **(required)**

      The issue submit date. The rquired format is `YYYY-MM-DD HH:MM`

    - `turnaroundTime: number` **(required)**

      The issue turnaround time.

    - `turnaroundTimeType: turnaroundTimeTypes` **default(`turnaroundTimeTypes.HOUR`)**

      The issue turnaround time type.

  - `returns: Object`

    - `name: string`

      The name of the reported issue

    - `deadLine: Date`

      The issue deadline date with the given format: `YYYY-MM-DD HH:MM`

### `turnaroundTimeTypes: enum`

- `HOUR: string`
- `DAY: string`
