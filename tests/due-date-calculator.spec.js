import { expect } from 'chai';
import {
  InvalidParameterError,
  InvalidDateError,
  InvalidInputTypeError,
  InvalidValueError
} from '../src/errors/index.js';
import DueDateCalculator from '../src/due-date-calculator';

const validReportInput = {
  name: 'bugReport',
  submitDate: '2019-2-3 12:21',
  turnaroundTime: 2
};

describe('DueDateCalculator tests', () => {
  describe('Consturctor tests', () => {
    it('Empty constructor should return with default values', () => {
      let dueDateCalculator = new DueDateCalculator();
      expect(dueDateCalculator.startTime).to.equal('9:00');
      expect(dueDateCalculator.endTime).to.equal('17:00');
    });

    it('With parameters should override the default values', () => {
      let dueDateCalculator = new DueDateCalculator({
        startTime: '8:00',
        endTime: '16:00'
      });
      expect(dueDateCalculator.startTime).to.equal('8:00');
      expect(dueDateCalculator.endTime).to.equal('16:00');
    });

    it('If the difference between startTime and endTime not equal 8 return InvalidValueError', () => {
      expect(
        () =>
          new DueDateCalculator({
            startTime: '8:00',
            endTime: '17:00'
          })
      ).to.throw(InvalidValueError);
      expect(
        () =>
          new DueDateCalculator({
            startTime: '12:00',
            endTime: '17:00'
          })
      ).to.throw(InvalidValueError);
    });

    it('The dueDateCalculator object should have a report() function', () => {
      let dueDateCalculator = new DueDateCalculator();
      expect(typeof dueDateCalculator.report).to.equals('function');
    });
  });
  let dueDateCalculator = new DueDateCalculator();
  describe('report() function tests', () => {
    it('Expect 1 object whit 3 mandatory parameters (name, submitDate, turnaroundTime ) as an input', () => {
      expect(() => dueDateCalculator.report()).to.throw(InvalidParameterError);
      expect(() => dueDateCalculator.report({ name: 'bugReport' })).to.throw(
        InvalidParameterError
      );
      expect(() =>
        dueDateCalculator.report({ name: 'bugReport', submitDate: '2019' })
      ).to.throw(InvalidParameterError);
      expect(() =>
        dueDateCalculator.report({ submitDate: '2019', turnaroundTime: 2 })
      ).to.throw(InvalidParameterError);
    });
    it('Throw an InvalidDateError if invalid submitDate given', () => {
      expect(() =>
        dueDateCalculator.report({
          name: 'bugReport',
          submitDate: '2019',
          turnaroundTime: 2
        })
      ).to.throw(InvalidDateError);
    });
    it('Throw an InvalidInputTypeError if the turnaroundTime param is not a number', () => {
      expect(() =>
        dueDateCalculator.report({
          name: 'bugReport',
          submitDate: '2019-2-3 18:21',
          turnaroundTime: 'string'
        })
      ).to.throw(InvalidInputTypeError);
    });
    it('Throw an InvalidInputTypeError if the turnaroundTimeType param value is not DAY or HOUR', () => {
      expect(() =>
        dueDateCalculator.report({
          ...validReportInput,
          turnaroundTimeType: 'NAP'
        })
      ).to.throw(InvalidInputTypeError);
    });
    it('Expect to be ok turnaroundTimeType param value is DAY or HOUR', () => {
      expect(
        dueDateCalculator.report({
          ...validReportInput,
          turnaroundTimeType: 'dAy'
        })
      ).to.be.ok;
      expect(
        dueDateCalculator.report({
          ...validReportInput,
          turnaroundTimeType: 'HOUR'
        })
      ).to.be.ok;
    });
    it('Expect to be ok if all manadtory parameters given with valid format', () => {
      expect(dueDateCalculator.report(validReportInput)).to.be.ok;
    });
    it('Throw an InvalidValueError error if the submitDate time does not between startTime and endTime', () => {
      expect(() =>
        dueDateCalculator.report({
          name: 'bugReport',
          submitDate: '2019-2-3 4:21',
          turnaroundTime: 2
        })
      ).to.throw(InvalidValueError);
      expect(() =>
        dueDateCalculator.report({
          name: 'bugReport',
          submitDate: '2019-2-3 21:21',
          turnaroundTime: 2
        })
      ).to.throw(InvalidValueError);
    });
    it('Should return whit the valid deadline', () => {
      const result = dueDateCalculator.report({
        name: 'bugReport',
        submitDate: '2019-8-7 13:21',
        turnaroundTime: 2,
        turnaroundTimeType: 'DAY'
      });
      const result2 = dueDateCalculator.report({
        name: 'bugReport2',
        submitDate: '2019-8-5 13:21',
        turnaroundTime: 3,
        turnaroundTimeType: 'DAY'
      });

      expect(result).to.be.ok;
      expect(result).to.have.property('name');
      expect(result).to.have.property('deadline');
      expect(result.deadline).to.equal('2019-8-9 13:21');
      expect(result2).to.be.ok;
      expect(result2).to.have.property('name');
      expect(result2).to.have.property('deadline');
      expect(result2.deadline).to.equal('2019-8-8 13:21');
    });
  });
});
