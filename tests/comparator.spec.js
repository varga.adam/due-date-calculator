import { expect } from 'chai';
import DueDateCalculator from '../src/due-date-calculator';
import { ComparatorError, InvalidParameterError } from '../src/errors';

const dueDateCalculator = new DueDateCalculator();

describe('Comparator tests', () => {
  describe('Constructor tests', () => {
    it('should have execute(), _between() functions', () => {
      expect(typeof dueDateCalculator.dateComparator.execute).to.equals(
        'function'
      );
      expect(typeof dueDateCalculator.dateComparator._between).to.equals(
        'function'
      );
    });
  });
  describe('execute() tests', () => {
    it('Should return ComparatorError if invalid function given for the first parameter', () => {
      expect(() => dueDateCalculator.dateComparator.execute('_test')).to.throw(
        ComparatorError
      );
    });
  });
  describe('_getMinutes(time) tests', () => {
    it('Should return with the minutus of the given time', () => {
      const minute = dueDateCalculator.dateComparator.execute(
        '_getMinutes',
        '18:20'
      );
      expect(minute).to.equal(18 * 60 + 20);
    });
  });

  describe('_hourInterval() tests', () => {
    it('Should return with true if the given hourInterval equal the result', () => {
      const result = dueDateCalculator.dateComparator.execute('_hourInterval', {
        startTime: '13:55',
        endTime: '21:55'
      });
      expect(result).to.equal(8);
    });
  });

  describe('_between() tests', () => {
    it('Should return true if actTime between startTime and endTime', () => {
      const result = dueDateCalculator.dateComparator.execute('_between', {
        startTime: '13:55',
        endTime: '21:55',
        actTime: '14:00'
      });
      expect(result).to.be.true;
    });

    it('Should return false if actTime greater then endTime', () => {
      const result = dueDateCalculator.dateComparator.execute('_between', {
        startTime: '13:55',
        endTime: '21:55',
        actTime: '22:00'
      });
      expect(result).to.be.false;
    });

    it('Should return false if actTime lower then startTime', () => {
      const result = dueDateCalculator.dateComparator.execute('_between', {
        startTime: '13:55',
        endTime: '21:55',
        actTime: '12:00'
      });
      expect(result).to.be.false;
    });
  });

  describe('_calculateTime() tests', () => {
    it('Should return a date that pushed foward with the given hours', () => {
      const result1 = dueDateCalculator.dateComparator.execute(
        '_calculateTime',
        {
          endTime: '17:00',
          submitDate: '2019-8-5 15:00',
          turnaroundTime: '2',
          turnaroundTimeType: 'HOUR'
        }
      );
      const result2 = dueDateCalculator.dateComparator.execute(
        '_calculateTime',
        {
          endTime: '17:00',
          submitDate: '2019-8-8 15:57',
          turnaroundTime: '64',
          turnaroundTimeType: 'HOUR'
        }
      );
      const result3 = dueDateCalculator.dateComparator.execute(
        '_calculateTime',
        {
          endTime: '17:00',
          submitDate: '2019-8-8 13:57',
          turnaroundTime: '56',
          turnaroundTimeType: 'HOUR'
        }
      );
      const result4 = dueDateCalculator.dateComparator.execute(
        '_calculateTime',
        {
          endTime: '17:00',
          submitDate: '2019-8-5 13:57',
          turnaroundTime: '64',
          turnaroundTimeType: 'HOUR'
        }
      );
      expect(result1).to.be.equal('2019-8-5 17:00');
      expect(result2).to.be.equal('2019-8-20 15:57');
      expect(result3).to.be.equal('2019-8-19 13:57');
      expect(result4).to.be.equal('2019-8-15 13:57');
    });
    it('Should return a date that pushed foward with the given days', () => {
      const result1 = dueDateCalculator.dateComparator.execute(
        '_calculateTime',
        {
          endTime: '19:57',
          submitDate: '2019-8-16 18:57',
          turnaroundTime: '1',
          turnaroundTimeType: 'DAY'
        }
      );
      const result2 = dueDateCalculator.dateComparator.execute(
        '_calculateTime',
        {
          endTime: '19:57',
          submitDate: '2019-8-13 18:57',
          turnaroundTime: '2',
          turnaroundTimeType: 'DAY'
        }
      );
      const result3 = dueDateCalculator.dateComparator.execute(
        '_calculateTime',
        {
          endTime: '19:57',
          submitDate: '2019-8-8 18:57',
          turnaroundTime: '17',
          turnaroundTimeType: 'DAY'
        }
      );
      expect(result1).to.be.equal('2019-8-19 18:57');
      expect(result2).to.be.equal('2019-8-15 18:57');
      expect(result3).to.be.equal('2019-9-2 18:57');
    });
  });
});
