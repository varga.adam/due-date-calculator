import {
  InvalidParameterError,
  InvalidDateError,
  InvalidInputTypeError,
  ComparatorError,
  InvalidValueError
} from './errors/index.js';
import { validateDate, validateTime, formatDate } from './tools/validations';
import { turnaroundTimeTypes, erroCodeTypes } from './enums';

/**
 * The class do the comparison between dates
 * Command Pattern
 */
class DateComparator {
  execute(command) {
    let args = Array.from(arguments).slice(1);
    if (this[command]) {
      return this[command].apply(this, args);
    } else {
      throw new ComparatorError('The command is not exists');
    }
  }
  /**
   * Calculate the deadline date for the issue.
   * @param {string} endTime the working hours end time
   * @param {Date} submitDate the submit date of the issue
   * @param {number} turnaroundTime the turnaround time of the issue
   * @param {turnaroundTimeTypes} turnaroundTimeType the turnaround time of the issue
   */
  _calculateTime({
    endTime,
    submitDate,
    turnaroundTime,
    turnaroundTimeType
  } = {}) {
    //Convert the submit date to date
    let submitDateDate = new Date(submitDate);
    //Convert the submit date to milisecond
    let resultDateInMilisecond = submitDateDate.getTime();

    let sumbitDateDayNumber =
      submitDateDate.getDay() != 0 ? submitDateDate.getDay() : 7;

    let addedMinutes;

    //Examine which type was given forturnaroundTimeType
    switch (turnaroundTimeType) {
      case turnaroundTimeTypes.HOUR:
        // Get the submit date hours and convert it to minutes
        let submitDateTimeMinutes = this._getMinutes(
          submitDate.slice(submitDate.indexOf(' ') + 1, submitDate.length)
        );

        // Convert the endTime to minutes
        let endTimeMinutes = this._getMinutes(endTime);

        // Get the quotient of the day (turnaroundTime >= 8)
        let quotientDay = Math.floor(turnaroundTime / 8);

        // Get the remained hours
        let remainderHour = turnaroundTime % 8;

        // Calculate how much time left from the working hour
        let untilTheEndMinutes = endTimeMinutes - submitDateTimeMinutes;

        // if there isn't quotientDay
        if (quotientDay < 1) {
          //Convert the turnaroundTime to minutes
          let turnaroundTimeMinutes = turnaroundTime * 60;

          // if untilTheEndMinutes is bigger then turnaroundTimeMinutes that means
          // you just need to add that minuts to the overall time, else
          // the abs(turnaroundTimeMinutes) gives back the left hours and
          // you just need to add it to the remaining hours until the start time
          addedMinutes =
            untilTheEndMinutes >= turnaroundTimeMinutes
              ? turnaroundTimeMinutes
              : Math.abs(turnaroundTimeMinutes) + (24 - 8) * 60;
        } else {
          let remainderMinutes = remainderHour * 60;

          // Convert quotientDay to minutes and
          addedMinutes = quotientDay * 24 * 60;

          // Same as you saw above. The difference is you need to calculate with remainderMinutes
          addedMinutes +=
            untilTheEndMinutes >= remainderMinutes
              ? remainderMinutes
              : Math.abs(remainderMinutes) + (24 - 8) * 60;
        }
        // Convert the addedMinutes to milisecond
        resultDateInMilisecond += addedMinutes * 60 * 1000;
        break;
      case turnaroundTimeTypes.DAY:
        addedMinutes = turnaroundTime * 24 * 60;
        // Convert the turnaroundTime to milisecond
        resultDateInMilisecond += addedMinutes * 60 * 1000;
        break;
    }

    // Compensation logic
    // because there is no working day at weekend you need to compensate
    // the weekend days

    //Convert the addedMinutes to day
    let addedMinutesInDay = Math.floor(addedMinutes / 60 / 24);

    // Examine how much week do we need to compensate
    let weekPast = Math.floor((sumbitDateDayNumber + addedMinutesInDay) / 6);

    // if we need compensate
    if (weekPast > 0) {
      // do it whit weekPast * 2 times (because Sunday and Saturday)
      resultDateInMilisecond += weekPast * 2 * 24 * 60 * 60 * 1000;
    }

    // Convert the result to date
    let resultDate = new Date(resultDateInMilisecond);

    // if the compensation land on Sunday or Saturday compensate 2 more day
    if (resultDate.getDay() === 0 || resultDate.getDay() === 6) {
      resultDate = new Date(resultDateInMilisecond + 2 * 24 * 60 * 60 * 1000);
    }

    // Return with the formatted date
    return formatDate(resultDate);
  }
  /**
   * Return true if the actTime between startTime and endTime
   * @param {string} startTime format->`HH:MM`
   * @param {string} endTime format->`HH:MM`
   * @param {string} actTime format->`HH:MM`
   */
  _between({ startTime, endTime, actTime } = {}) {
    const startTimeMinutes = this._getMinutes(startTime);
    const endTimeMinutes = this._getMinutes(endTime);
    const actTimeMinutes = this._getMinutes(actTime);

    if (startTimeMinutes > actTimeMinutes || endTimeMinutes < actTimeMinutes) {
      return false;
    }
    return true;
  }
  /**
   * Gives back the interval the given parameters
   * @param {string} startTime format->`HH:MM`
   * @param {string} endTime format->`HH:MM`
   */
  _hourInterval({ startTime, endTime }) {
    const startMinutes = this._getMinutes(startTime);
    const endMinutes = this._getMinutes(endTime);

    return (endMinutes - startMinutes) / 60;
  }

  /**
   * Gives back the minutes of the given time
   * @param {string} time foramt -> `HH:MM`
   */
  _getMinutes(time) {
    const hour = parseInt(time.slice(0, time.indexOf(':'))) * 60;
    const minute = parseInt(time.slice(time.indexOf(':') + 1, time.length));

    const result = minute + hour;
    return result;
  }
}

/**
 * The class do the issue reporting
 * @param {string} startTime foramt -> `HH:MM` default: '9:00'
 * @param {string} endTime foramt -> `HH:MM` default: '17:00'
 */
class DueDateCalculator {
  constructor({ startTime = '9:00', endTime = '17:00' } = {}) {
    // check if the given times are valid
    if (!validateTime(startTime) || !validateTime(endTime)) {
      throw new InvalidInputTypeError(
        'startTime and endTime must be a valid time string like 9:00 or 17:00!',
        erroCodeTypes.INVALID_CONSTRUCTOR_PARAMS
      );
    }
    this.dateComparator = new DateComparator();

    //check if the given times intervall is 8 hours
    if (
      this.dateComparator.execute('_hourInterval', { startTime, endTime }) != 8
    ) {
      throw new InvalidValueError(
        'it must be 8 hours between startTime and endTime'
      );
    }
    this.startTime = startTime;
    this.endTime = endTime;
  }

  /**
   *  Takes the issue name, submit date, turnaround time and turnaround time as an input and returns,
   *  the date and time when the issue is to be resolved
   * @param {string} name the issue name
   * @param {Date} submitDate the submit date format -> `YYYY-MM-DD HH:MM`
   * @param {number} turnaroundTime the turnaround time
   * @param {turnaroundTimeTypes} turnaroundTimeType the turnaround time type
   */
  report({
    name,
    submitDate,
    turnaroundTime,
    turnaroundTimeType = turnaroundTimeTypes.HOUR
  } = {}) {
    // Check the madantory parameters
    if (!name || !submitDate || !turnaroundTime) {
      throw new InvalidParameterError(
        'name, submitDate, turnaroundTime are mandatory parameters!',
        erroCodeTypes.INVALID_REPORT_PARAMS
      );
    }

    // Check if the date is valid
    if (!validateDate(submitDate)) {
      throw new InvalidDateError(
        'submitDate must be a valid date (YYYY-MM-dd)'
      );
    }

    //Check the turnaroundTime type is number
    if (typeof turnaroundTime !== 'number')
      throw new InvalidInputTypeError(
        'turnaroundTime must be an number!',
        erroCodeTypes.INVALID_TURNAROUND_TIME
      );

    //Check the turnaroundTimeType value is in the turnaroundTimeTypes enum
    if (!(turnaroundTimeType.toUpperCase() in turnaroundTimeTypes))
      throw new InvalidInputTypeError(
        'turnaroundTimeType must be DAY or HOUR!',
        erroCodeTypes.INVALID_TURNAROUND_TIME_TYPE
      );

    // Get the submitDate time part
    const submitDateTime = submitDate.slice(
      submitDate.indexOf(' ') + 1,
      submitDate.length
    );

    // Check if the submitDateTime berween startTime and endTime
    if (
      !this.dateComparator.execute('_between', {
        startTime: this.startTime,
        endTime: this.endTime,
        actTime: submitDateTime
      })
    ) {
      throw new InvalidValueError(
        'submitDate must be between startTime and endTime'
      );
    }

    // Calculate the deadline date
    const calculatedDate = this.dateComparator.execute('_calculateTime', {
      endTime: this.endTime,
      submitDate,
      turnaroundTime,
      turnaroundTimeType
    });

    return {
      name,
      deadline: calculatedDate
    };
  }
}

export default DueDateCalculator;
