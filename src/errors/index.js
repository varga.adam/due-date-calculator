/**
 * Thrown when invalid parameter given
 */
class InvalidParameterError extends Error {
  constructor(message, code) {
    super(message);
    this.code = code;
    this.name = 'InvalidParameterError';
  }
}

/**
 * Thrown when invalid input value given
 */
class InvalidValueError extends Error {
  constructor(message, code) {
    super(message);
    this.code = code;
    this.name = 'InvalidValueError';
  }
}

/**
 * Thrown when invalid date given
 */
class InvalidDateError extends Error {
  constructor(message) {
    super(message);
    this.name = 'InvalidDateError';
  }
}

/**
 * Thrown when when invalid input time given
 */
class InvalidInputTypeError extends Error {
  constructor(message, code) {
    super(message);
    this.code = code;
    this.name = 'InvalidInputTypeError';
  }
}

/**
 * Thrown when comparator gets an invalid param
 */
class ComparatorError extends Error {
  constructor(message, code) {
    super(message);
    this.code = code;
    this.name = 'ComparatorError';
  }
}

export default {
  InvalidParameterError,
  InvalidDateError,
  InvalidInputTypeError,
  ComparatorError,
  InvalidValueError
};
