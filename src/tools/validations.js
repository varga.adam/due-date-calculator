/**
 * Format the given date to `YYYY:MM:DD HH:MM` format
 * @param {*} date
 */
export const formatDate = (date) => {
  let day = date.getDate();
  let monthIndex = date.getMonth();
  let year = date.getFullYear();
  let hour = date.getHours();
  let minutes = date.getMinutes();
  let formatedMinutes = minutes != 0 ? minutes : `${minutes}0`;

  return `${year}-${monthIndex + 1}-${day} ${hour}:${formatedMinutes}`;
};

/**
 * Validate the given date. The string format must be `YYYY:MM:DD HH:MM`
 * @param {string} date
 */
export const validateDate = (date) => {
  const regex = /^\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01]) (00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$/;

  if (date != '' && !date.match(regex)) {
    return false;
  }
  return true;
};

/**
 * Validate the given time. The string format must be `HH:MM`
 * @param {string} time
 */
export const validateTime = (time) => {
  const regex = /^(00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$/;

  if (time != '' && !time.match(regex)) {
    return false;
  }
  return true;
};
